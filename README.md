# BulkStatus Online

Comprobador web de estados de URL orientado como herramienta SEO.

**AUTORES**

Alberto José Martínez Mora 100330775    
Sara Jiménez Flores 100330185

**PROPÓSITO DEL PROYECTO**

El objetivo que persigue este proyecto es la creación de una herramienta online para la comprobación de códigos de estado, principalmente, de URLs para facilitar el trabajo SEO (Search Engine Optimization).

**HERRAMIENTAS UTILIZADAS**

- Google Cloud Platform
- FileZilla para la gestión de archivos
- [Hostalia](https://www.hostalia.com/dominios/?gclid=CjwKCAiA9bmABhBbEiwASb35V2DCABx21TQSN4wWGTRdY2912nHtL6U1xlTP3uXVti_hp_Nz3VYNjRoCSYgQAvD_BwE#dominio-com) para la compra del dominio
- [Canva](https://www.canva.com/) para el diseño del logo
- Proxy CORS
- CMS Wordpress.org
- Search Console

**LENGUAJE DE PROGRAMACIÓN**

- PHP
- HTML
- JavaScript
- CSS

**DESARROLLO DEL PROYECTO**

1. Compra de dominio bulkstatus.online
2. Alojamiento del dominio en la Google Cloud
3. Instalación de certificado de seguridad HTTPS
4. Implementación de WordPress
5. Configuración SSH de acceso a archivos a través de FileZilla
6. Creación de una landing page con HTML, JavaScript y API FETCH
7. Mejora del aspecto visual de la web con CSS
8. Testeo y corrección de fallos durante todo el proyecto

**RESULTADOS OBTENIDOS**

Este proyecto ha dado como resultado una herramienta online que permite comprobar los estados HTTP de una URL introducida. Además, se han añadido otros campos interesantes como Content-Type, Content-Length y Date como mejora del proyecto base.

BulkStatus consigue facilitar el trabajo de una persona que se dedique al SEO, o posicionamiento web. Con tan solo una consulta en el buscador se puede obtener el código que devuelve el protocolo HTTP. De esta manera, es muy sencillo comprobar si una URL tiene un código 404, que es el que indica que necesita solucionarse.

**POSIBLES MEJORAS**

1. Añadir más campos de consulta
2. Adaptarlo para una extensión de Chrome
3. Convertir esta herramienta en una aplicación para facilitar aún más la consulta
